import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;

public class TesteSicrediDesafio2 {

    private WebDriver driver;
    private DSL dsl;

    @Before
    public void inicializa_drive() {
        System.setProperty("webdriver.chrome.driver", "src/test/java/Chromedriver/chromedriver.exe");
        driver = new ChromeDriver();
        driver.manage().window().maximize();
        driver.get("https://www.grocerycrud.com/v1.x/demo/my_boss_is_in_a_hurry/bootstrap");
        dsl = new DSL(driver);
    }
    @After
    public void fechar_driver() {
        driver.quit();
    }

    @Test
    public void PassosDoDesafio1() throws InterruptedException {
        WebElement element = driver.findElement(By.id("switch-version-select"));
        Select combo = new Select(element);
        combo.selectByVisibleText("Bootstrap V4 Theme");
        dsl.clicarLink("Add Record");
        dsl.escreve("field-customerName", "Teste Sicredi");
        dsl.escreve("field-contactLastName", "Teste");
        dsl.escreve("field-contactFirstName", "Ricardo Batista De Castro");
        dsl.escreve("field-phone", "11 973185149");
        dsl.escreve("field-addressLine1", "Av Assis Brasil, 3970");
        dsl.escreve("field-addressLine2","Torre D");
        dsl.escreve("field-city","Porto Alegre");
        dsl.escreve("field-state","RS");
        dsl.escreve("field-postalCode","91000-000");
        dsl.escreve("field-country","Brasil");
        dsl.escreve("field-salesRepEmployeeNumber","123");
        dsl.escreve("field-creditLimit","200");
        dsl.clicarID("form-button-save");
        Thread.sleep(3000);
        dsl.clicarLink("Go back to list");
        dsl.digitaName("customerName","Teste Sicredi");
        Thread.sleep(2000);
        dsl.clicarCheckBox("select-row");
        Thread.sleep(2000);
        dsl.clicarCheckBox("select-row");
        dsl.clicarClasseName("text-danger");
        Thread.sleep(1000);
        dsl.clicarxpacth("/html/body/div[2]/div[2]/div[3]/div/div/div[3]/button[2]");
        Thread.sleep(2000);
        Assert.assertEquals("Your data has been successfully deleted from the database.",
                driver.findElement(By.xpath("/html/body/div[3]/span[3]/p")).getText());

    }

}

