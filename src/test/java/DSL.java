import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class DSL {

    private WebDriver driver;

    public DSL(WebDriver driver){
        this.driver = driver;
    }

    public void escreve (String id_campo, String texto){

        driver.findElement(By.id(id_campo)).sendKeys(texto);
    }

    public void clicarLink(String link){
        driver.findElement(By.linkText(link)).click();
    }
    public void clicarID(String id){
        driver.findElement(By.id(id)).click();
    }
    public void digitaName(String campoName, String digitar){
        driver.findElement(By.name(campoName)).sendKeys(digitar);
    }
    public void clicarCheckBox(String checkbox){
        driver.findElement(By.className(checkbox)).click();
    }
    public void clicarClasseName(String className){
        driver.findElement(By.className(className)).click();
    }
    public void clicarxpacth(String xpath){
        driver.findElement(By.xpath(xpath)).click();
    }

}

